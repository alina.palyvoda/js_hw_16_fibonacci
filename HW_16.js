/**
 * Created by Алина on 15.01.2022.
 */

let userNumber = +prompt("Enter your number");

function getFibonacci(n) {
    if (n === 0) {
        return 0
    }
    if (n === 1) {
        return 1
    }
    if (n < 0) {
        return getFibonacci(n + 2) - getFibonacci(n + 1)
    } else {
        return getFibonacci(n - 1) + getFibonacci(n - 2)
    }
}

let fibonacci = getFibonacci(userNumber);
console.log(fibonacci);

